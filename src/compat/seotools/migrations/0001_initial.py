# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='UrlData',
            fields=[
                ('url', models.CharField(primary_key=True, serialize=False, max_length=255, help_text='\u0444\u043e\u0440\u043c\u0430\u0442 \u0432\u0432\u043e\u0434\u0430 url: "/ru/projects/"', unique=True, verbose_name='url')),
                ('title', models.CharField(max_length=128, null=True, verbose_name='title', blank=True)),
                ('keywords', models.TextField(max_length=600, null=True, verbose_name=b'keywords', blank=True)),
                ('description', models.TextField(max_length=600, null=True, verbose_name='description', blank=True)),
            ],
            options={
                'verbose_name': 'Url \u0434\u0430\u043d\u043d\u044b\u0435',
                'verbose_name_plural': 'Url \u0434\u0430\u043d\u043d\u044b\u0435',
            },
            bases=(models.Model,),
        ),
    ]
