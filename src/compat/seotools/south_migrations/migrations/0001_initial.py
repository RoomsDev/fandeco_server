# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'UrlData'
        db.create_table(u'seotools_urldata', (
            ('url', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255, primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('keywords', self.gf('django.db.models.fields.TextField')(max_length=600)),
            ('description', self.gf('django.db.models.fields.TextField')(max_length=600)),
        ))
        db.send_create_signal(u'seotools', ['UrlData'])


    def backwards(self, orm):
        # Deleting model 'UrlData'
        db.delete_table(u'seotools_urldata')


    models = {
        u'seotools.urldata': {
            'Meta': {'object_name': 'UrlData'},
            'description': ('django.db.models.fields.TextField', [], {'max_length': '600'}),
            'keywords': ('django.db.models.fields.TextField', [], {'max_length': '600'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'url': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255', 'primary_key': 'True'})
        }
    }

    complete_apps = ['seotools']