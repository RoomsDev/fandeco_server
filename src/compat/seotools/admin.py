# -*- coding: utf-8 -*-

from .models import UrlData
from django.contrib import admin


class UrlDataAdmin(admin.ModelAdmin):
    list_display = ('url', 'title', 'keywords')
    search_fields = ('url', 'title',)

admin.site.register(UrlData, UrlDataAdmin)
