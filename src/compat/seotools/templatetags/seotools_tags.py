# -*- coding: utf-8 -*-

# note:
# django.core.context_processors.request must be included to TEMPLATE_CONTEXT_PROCESSORS
# for this module to work

# urls are kept in Title table with first and last slashes stripped-out
# with exception of the root url which is '/'

from seotools.models import UrlData
from django import template
from django.core.exceptions import ObjectDoesNotExist

register = template.Library()

class TitleNode(template.Node):

    def __init__(self, varName):
        self.varName = varName

    def render(self, context):
        if context.has_key('request'):
            path = context['request'].path
        else:
            path = ''
        if path == '': # root url
            path = '/' # admin does not like empty keys
        try:
            obj = UrlData.objects.get(url=path)
            title = obj.title
        except ObjectDoesNotExist:
            title = ''
        context[self.varName] = title
        return ''

class KeywordsNode(template.Node):

    def __init__(self, varName):
        self.varName = varName

    def render(self, context):
        if context.has_key('request'):
            path = context['request'].path
        else:
            path = ''
        if path == '': # root url
            path = '/' # admin does not like empty keys
        try:
            obj = UrlData.objects.get(url=path)
            title = obj.keywords
        except ObjectDoesNotExist:
            title = ''
        context[self.varName] = title
        return ''

class DescriptionNode(template.Node):

    def __init__(self, varName):
        self.varName = varName

    def render(self, context):
        if context.has_key('request'):
            path = context['request'].path
        else:
            path = ''
        if path == '': # root url
            path = '/' # admin does not like empty keys
        try:
            obj = UrlData.objects.get(url=path)
            title = obj.description
        except ObjectDoesNotExist:
            title = ''
        context[self.varName] = title
        return ''

class SeoMetaNode(template.Node):

    def __init__(self, varName):
        self.varName = varName

    def render(self, context):
        if context.has_key('request'):
            path = context['request'].path
        else:
            path = ''
        if path == '': # root url
            path = '/' # admin does not like empty keys
        try:
            obj = UrlData.objects.get(url=path)
        except ObjectDoesNotExist:
            obj = None
        context[self.varName] = obj
        return ''

@register.tag
def get_title_for_this_page(parser, token):
    def fail():
        tag = token.contents.split()[0]
        raise template.TemplateSyntaxError(
                '%r tag must be in form: "%s as <var-name>"' % (tag, tag))
    try:
        tagName, as_, varName = token.contents.split()
    except ValueError:
        fail()
    if as_ != 'as':
        fail()
    return TitleNode(varName)

@register.tag
def get_keywords_for_this_page(parser, token):
    def fail():
        tag = token.contents.split()[0]
        raise template.TemplateSyntaxError(
                '%r tag must be in form: "%s as <var-name>"' % (tag, tag))
    try:
        tagName, as_, varName = token.contents.split()
    except ValueError:
        fail()
    if as_ != 'as':
        fail()
    return KeywordsNode(varName)

@register.tag
def get_description_for_this_page(parser, token):
    def fail():
        tag = token.contents.split()[0]
        raise template.TemplateSyntaxError(
                '%r tag must be in form: "%s as <var-name>"' % (tag, tag))
    try:
        tagName, as_, varName = token.contents.split()
    except ValueError:
        fail()
    if as_ != 'as':
        fail()
    return DescriptionNode(varName)


@register.tag
def get_seometa_for_this_page(parser, token):
    def fail():
        tag = token.contents.split()[0]
        raise template.TemplateSyntaxError(
                '%r tag must be in form: "%s as <var-name>"' % (tag, tag))
    try:
        tagName, as_, varName = token.contents.split()
    except ValueError:
        fail()
    if as_ != 'as':
        fail()
    return SeoMetaNode(varName)
