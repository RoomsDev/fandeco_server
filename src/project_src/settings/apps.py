# -*- coding: utf-8 -*-
# Django settings for project apps.
import os
from .base import MEDIA_ROOT

# for django-admin-tools
ADMIN_TOOLS_MENU = 'project_src.at_menu.CustomMenu'
ADMIN_TOOLS_INDEX_DASHBOARD = 'project_src.at_dashboard.CustomIndexDashboard'
ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'project_src.at_dashboard.CustomAppIndexDashboard'

# Cache settings
# CACHE_MIDDLEWARE_ANONYMOUS_ONLY = True
CACHE_SHORT_PERIOD = 60 # Seconds
CACHE_LONG_PERIOD = 500
CACHE_EXTRA_PERIOD = 120

THUMBNAIL_SUBDIR = "__thumbs__"

USERPROFILES_EMAIL_ONLY = True
USERPROFILES_USE_ACCOUNT_VERIFICATION = True
USERPROFILES_REGISTRATION_FULLNAME = True


########### CKEditor settings ###########
CKEDITOR_RESTRICT_BY_USER = True
CKEDITOR_IMAGE_BACKEND = 'pillow'
CKEDITOR_UPLOAD_PATH = os.path.join(MEDIA_ROOT, 'fb/uploads')

CKEDITOR_CONFIGS = {
    'default': {
        'skin': 'bootstrapck',  # moono, bootstrapck, moonocolor
        'toolbar': 'Basic',
        'language': 'ru',
        'toolbar_Basic': [
             ['Undo', 'Redo', '-', 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList', ],
             # ['Styles', 'Format', 'Undo', 'Redo', '-', 'Bold', 'Italic', 'Underline', 'Strike', '-', 'SpellChecker', 'NumberedList', 'BulletedList', ],
             # ['Image', 'Flash', 'Table', 'HorizontalRule'],
             # ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'HorizontalRule', 'Blockquote'],
             # [ 'TextColor', 'BGColor'],
             # ['Link', 'Unlink', 'Anchor', ],
             # ['Link', 'Unlink', ],
             # ['Smiley', 'SpecialChar'],
             ['Source'],
#              '-', 'Font', 'FontSize',
#              '-', 'Outdent', 'Indent',
#             ]
        ],
        'forcePasteAsPlainText': True,
        'resize_enabled': True,
        'height':'210',
        'width': '930',
    },

    'admin_ckeditor': {
        'skin': 'bootstrapck',  # moono, bootstrapck, moonocolor
        'toolbar': 'Full',
        'toolbar_Full': [
             # ['Undo', 'Redo', '-', 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList', ],
              ['Styles', 'Format', '-' ,
              'Link', 'Unlink', 'Anchor', '-', 'File', 'Image', 'Flash', 'Table', '-',
              'Smiley', 'SpecialChar', '-', 'Source', ],

              # [],
              ['Undo', 'Redo', '-', 'Bold', 'Italic', 'Underline', 'Strike', '-',
                'SpellChecker', 'NumberedList', 'BulletedList', '-',
               'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-'
               'HorizontalRule', 'Blockquote', '-',
               'Outdent', 'Indent'],

             # [ 'TextColor', 'BGColor'],
              # ['Link', 'Unlink', 'Anchor', ],
            # ['Link', 'Unlink', ],
             # ['Smiley', 'SpecialChar'],

#              '-', 'Font', 'FontSize',
#              '-', 'Outdent', 'Indent',
#             ]
        ],
        'language': 'ru',
        'forcePasteAsPlainText': True,
        'resize_enabled': True,
    },
}


