
from .local import *

SOUTH_TESTS_MIGRATE = False

FIXTURE_DIRS = (
    os.path.join(os.path.dirname(PROJECT_ROOT), 'fixtures'),
)

# for captcha
CAPTCHA_TEST_MODE = True

# disabled migrations only when tests are running from 1.7
class DisableMigrations(object):

    def __contains__(self, item):
        return True

    def __getitem__(self, item):
        return "notmigrations"


PASSWORD_HASHERS = (
    'django.contrib.auth.hashers.MD5PasswordHasher',
)
DEBUG = False
TEMPLATES[0]['OPTIONS']['debug'] = False
TESTS_IN_PROGRESS = True
MIGRATION_MODULES = DisableMigrations()
