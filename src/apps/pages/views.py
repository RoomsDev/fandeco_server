# Create your views here.
from django.views.generic import DetailView, ListView

from .models import Page


class PageListView(ListView):
    template_name = 'pages/page_list.html'
    context_object_name = 'page_list'

    def get_queryset(self):
        return Page.objects.all_published()


class PageDetailView(DetailView):
    template_name = 'pages/page_detail.html'
    queryset = Page.objects.all_published()

