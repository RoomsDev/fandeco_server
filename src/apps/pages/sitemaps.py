from django.contrib.sitemaps import Sitemap

from .models import Page


class PageSitemap(Sitemap):
    changefreq = "monthly"

    def __init__(self, language):
        self.language = language

    def items(self):
        return Page.objects.all_published().filter(is_in_sitemap=True)

    def location(self, obj):
        # change locale in the url for the language for this sitemap
        try:
            from localeurl.templatetags.localeurl_tags import chlocale
            return chlocale(obj.get_absolute_url(), self.language)
        except:
            return obj.get_absolute_url()


