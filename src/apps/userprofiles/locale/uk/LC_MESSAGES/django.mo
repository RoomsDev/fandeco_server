��    \      �     �      �     �     �  +        9  )   F     p          �     �     �  	   �     �     �     �     �  *   �     $	     :	     G	     W	     f	     k	     z	     �	  #   �	     �	     �	     �	     �	     
  
   

     
     
     7
  %   T
     z
  	   �
  
   �
     �
     �
  	   �
  
   �
  
   �
     �
  &   �
          5     H     Q     c  c   i  e   �     3     B     R     _     u     �     �  �   �  8     E   M  %   �  N   �       B         c  >   i     �     �     �     �     �  @   �  t     y   �  &     '   .     V  *   l  !   �  �   �     �     �  	   �  
   �     �     �     �     �  *     �  6  !        2  X   Q     �  A   �       4     #   I  #   m     �     �     �     �     �       b   +  =   �  #   �     �          *     1     O     V  1   r  &   �     �     �  1   �     1     F     N     f  (   ~  6   �  H   �     '  
   8  
   C  !   N     p     �     �     �  C   �  C        ]     |  !   �     �  �   �  �   d  1        5     S  '   h     �  
   �     �  �   �  �   �  z   D  4   �  �   �  )   �  l   �     #   s   ,      �   !   �   %   �      �      !  {   -!  �   �!  �   �"  0   d#  ;   �#  *   �#  X   �#  <   U$  t  �$     &  !   
&     ,&     I&     _&  H   n&     �&     �&  p   �&     Z   7      <          -      4   (   9   M              3              8   /   '           !          [   1                            N                     I          	   C       W          >      .   X                      "   F      V      #              P       
   Q         R   ?       $       )   &   S             6           T          5   +   @   A   E   O   Y         =   K      %   :   0       B   \   ,   H   D   *      L      G                   U   ;   J   2    %(count)d records were %(email)s already activated. %(msg)s successfully send activation email. 1 record was A user with that username already exists. About yourself Account activation Account verification Account verifications Activation key Add image Add shop Address Approved Birthday By clicking 'Create account' I agree that: Change e-mail address Change image Change password Change profile Code Create account E-mail E-mail (repeat) E-mail address changed to %(email)s E-mail confirmation E-mail verification E-mail verifications Expiration date Expired First name Go to I am at least 18 years old. I have read and accepted the In order to add a review, you should  In order to vote, you should  Last name Logged out Login Mobile phone My orders My profile My reviews New e-mail address No Password? Reset your password here. No account? Register here. Old e-mail address Password Password (repeat) Phone Please click on the following link to change your email address:

http://%(domain)s%(url)s

Cheers! Please click on the following link to reset your password:

%(protocol)s://%(domain)s%(url)s

Cheers! Privacy Policy Profile changed Registration Registration complete Reset password Sex Sign in Thank you for creating an account.

Please click on the following link to activate your account:

http://%(domain)s%(url)s

Cheers! The reset link is invalid. Please double check the link. The submitted activation code is invalid. Activation is not possible. The two email addresses do not match. This email address is already in use. Please supply a different email address. This field is required. This value may contain only letters, numbers and ./-/_ characters. Token Unable to change e-mail address. Confirmation link is invalid. User User Agreement User profile Username View We activated your account. You are now able to log in. Have fun! We send you a e-mail including a link. Please click the
        link to continue resetting your password. Thank you! We send you a e-mail including a link. Please click the link to
        continue changing your e-mail address. Thank you! We successfully changed your password. We successfully resetted your password. You are not logged in You must type the same password each time. Your registration was successful. Your registration was successful. We send you a e-mail including a link.<br />
	            Please click the link to activate your account. Thank you!<br />
	            <br />
	            The link is valid for %(expiration_days)s days. and authorization form authorize created at home opens in a new window or tab. or registration form resend activation email for selected users Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-07-01 23:47+0300
PO-Revision-Date: 2014-06-20 14:01+0202
Last-Translator:    <admin@admin.com>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Translated-Using: django-rosetta 0.7.4
 %(count)d записів були %(email)s активовані %(msg)s успішно відправлено лист з кодом активації. 1 запису було Користувач із таким ім'ям вже існує. Про себе Активація облікового запису Перевірка аккаунту Перевірки аккаунту Ключ активації Додати картинку Додати магазин Адреса підтверджено День народження Натиснувши кнопку 'Створити акаунт' я згоден з тим, що: Змінити адресу електронної пошти Змінити зображення Змінити пароль Змінити профіль Код Створити акаунт E-mail E-mail (повторити) E-mail адресу змінено на %(email)s Підтвердження по E-mail E-mail Перевірка E-mail Перевірки Дата закінчення строку дії Закінчився Ім'я Перейдіть до Мені 16 років. Я прочитав і Я прийняв Щоб додати відгук, ви повинні  Для того, щоб проголосувати, ви повинні  Прізвище Вийти Логін Мобільний телефон Мої замовлення Мій профіль Мої відгуки Нова e-mail адреса Без Пароля? Відновити Ваш пароль тут. Немає аккаунта? Зареєструватися тут. Стара e-mail адреса Пароль Пароль (повторити) Телефон Будь-ласка, натисніть на посилання, щоб змінити свою адресу електронної пошти:

http://%(domain)s%(url)s Будь-ласка, натисніть на наступне посилання для відновлення паролю:

%(protocol)s://%(domain)s%(url)s Політики Конфіденційності Змінено профіль Реєстрація Реєстрація завершена Скинути пароль Стать Ввійти Дякуємо за створення облікового запису.

Будь-ласка, натисніть на наступне посилання для активації облікового запису:

http://%(domain)s%(url)s Посилання для відновлення, є неприпустимим. Будь ласка, перевірте посилання. Представлений код активації є неприпустимим. Активація неможлива. Два email-адреси не збігаються. Ця адреса вже використовується. Будь ласка, введіть іншу адресу електронної пошти. Це поле є обов'язковим. Це значення може містити тільки літери, цифри і ./-/_ символи. Ключ Не вдається змінити e-mail. Посилання для підтвердження недійсне. Користувач Угоду Користувача Профіль користувача Ім'я користувача Перегляд Ми активували Ваш акаунт. Тепер Ви можете увійти в систему. Вітаємо! Ми відправимо вам e-mail включаючи посилання. Будь-ласка, натисніть на 
посилання на продовження скидання пароля. Дякуємо! Ми відправимо вам e-mail включаючи посилання. Будь-ласка, натисніть посилання
продовжити змінювати вашу e-mail адресу. Спасибі! Ми успішно змінили пароль. Ми успішно відновили Ваш пароль. Ви не ввійшли в систему Ви повинні ввести той самий пароль кожного разу. Ваша реєстрація пройшла успішно. Ваша реєстрація пройшла успішно. Ми відправимо вам e-mail включаючи посилання.<br /> 
Будь-ласка, натисніть на посилання для активації облікового запису. Спасибі!;<br /><br /> 
Посилання дійсне лише %(expiration_days)s днів. і форми авторизації авторизуватися створений в головна відкриється у новому вікні або вкладці. або форма реєстрації повторно відіслати лист для активації вибраних користувачів 