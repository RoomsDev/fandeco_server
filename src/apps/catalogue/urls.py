from django.conf.urls import include, url
from django.contrib import admin
from .views import CatalogueView, PriceListView

urlpatterns = [
  url(r'^$', PriceListView.as_view(), name='index'),
]
