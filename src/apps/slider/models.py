# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _


class HSlider(models.Model):
    name = models.CharField(_("name"), max_length=255)
    caption = models.CharField(_("text"), max_length=255, blank=True)
    image = models.ImageField(_("image"), upload_to='slider/images/%Y/%m/%d',)
    url = models.CharField(_("url"), max_length=255, blank=True)
    created_at = models.DateTimeField(_('created at'), auto_now_add=True)
    rate = models.IntegerField(_("sorting"), default=0, help_text=_("The larger the value, the slider above"))
    is_published = models.BooleanField(default=True, verbose_name=_('published'))

    class Meta:
        ordering = ['created_at']
        verbose_name = _("Slider")
        verbose_name_plural = _("Sliders")

    def __unicode__(self):
        return '%s' % self.name
