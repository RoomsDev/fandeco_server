# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='\u0418\u043c\u044f \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u0438')),
                ('domain', models.CharField(max_length=100, verbose_name='\u0414\u043e\u043c\u0435\u043d')),
            ],
            options={
                'verbose_name': '\u041a\u043e\u043c\u043f\u0430\u043d\u0438\u044f',
                'verbose_name_plural': '\u041a\u043e\u043c\u043f\u0430\u043d\u0438\u0438',
            },
        ),
    ]
