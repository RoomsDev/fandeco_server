from django import forms
from django.utils.translation import ugettext_lazy as _
from reviews.models import Review, Vote
from django.contrib.contenttypes.models import ContentType


class ReviewTextForm(forms.ModelForm):
    score = forms.IntegerField(widget=forms.HiddenInput(), required=False)

    class Meta:
        model = Review
        fields = ('advantages_html', 'disadvantages_html', 'body_html')


class ReviewForm(ReviewTextForm):
    content_id = forms.IntegerField(widget=forms.HiddenInput())
    content_type = forms.IntegerField(widget=forms.HiddenInput())

    def __init__(self, object, *args, **kwargs):
        content_type = ContentType.objects.get_for_model(object)
        super(ReviewForm, self).__init__(*args, **kwargs)
        if object:
            self.fields['content_id'].widget.attrs = {'value':object.pk}
            self.fields['score'].widget.attrs = {'value':0}
            self.fields['content_type'].widget.attrs = {'value':content_type.pk}


class VoteForm(forms.ModelForm):

    class Meta:
        model = Vote
        fields = ('delta',)

    def __init__(self, review, user, *args, **kwargs):
        super(VoteForm, self).__init__(*args, **kwargs)
        self.instance.review = review
        self.instance.user = user

    @property
    def is_up_vote(self):
        return self.cleaned_data['delta'] == Vote.UP

    @property
    def is_down_vote(self):
        return self.cleaned_data['delta'] == Vote.DOWN
