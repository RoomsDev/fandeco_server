from django.contrib import admin

from reviews.models import Review, Vote


@admin.register(Review)
class ReviewAdmin(admin.ModelAdmin):
    list_display = ('user', 'content_type', 'score', 'status', 'count_votes_up',
                    'count_votes_down', 'created_at')


@admin.register(Vote)
class VoteAdmin(admin.ModelAdmin):
    list_display = ('review', 'user', 'delta', 'created_at')
