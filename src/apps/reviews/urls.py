# -*- coding: utf-8 -*-

from django.conf.urls import url, patterns

from reviews.views import add_review, vote_reviews

urlpatterns = patterns("",

    url(r'^add/$', add_review, name='reviews_form',),
    url(r'^vote/(?P<pk>\d+)/add$', vote_reviews, name='vote_reviews',),

)
