from django.db import models
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import ugettext_lazy as _

from reviews.managers import ApprovedReviewsManager
from reviews.settings import r_settings


class Review(models.Model):
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField(_('ID object'))
    SCORE_CHOICES = tuple([(x, x) for x in range(0, 6)])
    score = models.SmallIntegerField(_("Score"), choices=SCORE_CHOICES, default=0)
    advantages_html = models.TextField(_('Advantages'), blank=True)
    disadvantages_html = models.TextField(_('Disadvantages'), blank=True)
    body_html = models.TextField(_('HTML version'))
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name='reviews', null=True, blank=True)
    FOR_MODERATION, APPROVED, REJECTED = list(range(0, 3))
    STATUS_CHOICES = (
        (FOR_MODERATION, _("Requires moderation")),
        (APPROVED, _("Approved")),
        (REJECTED, _("Rejected")),
    )
    default_status = APPROVED
    if r_settings.MODERATE_REQUIRE:
        default_status = FOR_MODERATION
    status = models.SmallIntegerField(
        _("Status"), choices=STATUS_CHOICES, default=default_status)
    count_votes_up = models.PositiveIntegerField(verbose_name=_('up votes'), default=0)
    count_votes_down = models.PositiveIntegerField(verbose_name=_('down votes'), default=0)
    created_at = models.DateTimeField(auto_now_add=True)

    # Managers
    objects = models.Manager()
    approved = ApprovedReviewsManager()

    class Meta:
        ordering = ['-created_at']
        verbose_name = _('Review')
        verbose_name_plural = _('Reviews')

    def __unicode__(self):
        return "%s-%s" % (self.content_type.name, self.object_id)

    @property
    def get_stars_score(self):
        return self.score * 17


class Vote(models.Model):
    review = models.ForeignKey(Review, related_name='votes')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='votes')
    UP, DOWN = 1, -1
    VOTE_CHOICES = (
        (UP, _("Up")),
        (DOWN, _("Down"))
    )
    delta = models.SmallIntegerField(_('Delta'), choices=VOTE_CHOICES)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-created_at']
        verbose_name = _('Vote')
        verbose_name_plural = _('Votes')

