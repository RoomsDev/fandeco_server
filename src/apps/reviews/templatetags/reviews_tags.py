# -*- coding: utf-8 -*-

from django import template
from django.contrib.contenttypes.models import ContentType

from reviews.forms import ReviewForm
from reviews.models import Review

register = template.Library()


@register.inclusion_tag('reviews/tags/list_reviews_by_object.html', takes_context=True)
def get_reviews_by_object(context, object):
    content_type = ContentType.objects.get_for_model(object)
    reviews = Review.approved.filter(
        content_type=content_type, object_id=object.pk).order_by('-created_at').select_related('user')
    return {'reviews':reviews, 'request':context['request']}


@register.inclusion_tag('reviews/tags/reviews_form.html', takes_context=True)
def get_reviews_form(context, object):
    return {'form':ReviewForm(object), 'request':context['request']}
