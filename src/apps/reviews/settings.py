# -*- coding: utf-8 -*-
from django.conf import settings


class Settings(object):
    def __init__(self, **kwargs):
        self.defaults = kwargs

    def __getattr__(self, key):
        return getattr(settings, 'REVIEWS_%s' % key, self.defaults[key])


r_settings = Settings(
    MODERATE_REQUIRE=True,
)
