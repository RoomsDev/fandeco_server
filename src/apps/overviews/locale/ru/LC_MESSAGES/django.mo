��    2      �      <      <     =     J  )   X     �     �     �     �     �     �     �     �     �     �  	   �     �     
          $     -  	   A  -   K     y     �  	   �     �     �     �     �     �     �     �                #  
   *     5     <     H     N     S     Y     f  	   t  	   ~     �     �     �     �  
   �  �  �     �     �  C   �               0     A     R  %   m  &   �     �     �     �     �  "   	     4	     H	  
   ^	     i	     �	  e   �	     �	  0   	
     :
     S
  *   k
  &   �
     �
  :   �
       !   #  9   E       
   �     �     �     �     �     �     �               .     F     _     {     �     �     �   Add overview Add overview  Are you sure you want to delete overview? Delete Delete overview Detail Draft Edit Edit overview Edit overview  Filter HTML version Latest overviews Moderated Moderated overview My overviews No overviews Overview Overview in product Overviews Please correct the errors below and try again Preview Preview  Published Read overview Send for moderation Show per page Status This field is required. Title overview Title product Used to build the entry's URL. Yes author created at delete description draft edit image is a deleted is send email moderated published published at slug status title updated at Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-07-29 02:55+0300
PO-Revision-Date: 2014-07-29 02:58+0202
Last-Translator:    <admin@admin.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: 
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Translated-Using: django-rosetta 0.7.4
 Добавить обзор Добавить обзор  Вы уверены, что хотите удалить обзор? Удалить Удалить обзор Подробно Черновик Редактировать Редактировать обзор Редактировать обзор  Фильтр HTML версия Последние обзоры На Модерации Обзор на модерацию Мои обзоры Нет обзоров Обзор Обзор продукта Обзоры Пожалуйста, исправьте ошибки ниже, и попробуйте еще раз Просмотр Предварительный просмотр  Опубликовано Читать обзор Отправить на модерацию Показать на странице Статус Это поле является обязательным. Название обзора Название продукта Используется для построения URL. Да автор создано в удалить описание черновик редактировать изображение удалено отправлено email на модерации опубликовано опубликовано в ЧПУ статус название обновлено в 