# -*- coding: utf-8 -*-

from django import forms
from ckeditor.widgets import CKEditorWidget
from overviews.models import Overview


class OverviewPostForm(forms.ModelForm):

    class Meta:
        model = Overview
        fields = ['title', 'body_html', 'image']

    def __init__(self, *args, **kwargs):
        super(OverviewPostForm, self).__init__(*args, **kwargs)
        self.fields['title'].widget.attrs = {'required':'required'}
        self.fields['body_html'].widget = CKEditorWidget()
        self.fields['body_html'].widget.attrs = {'required':'required'}

