from django.contrib.sitemaps import Sitemap
from models import Overview

class OverviewSitemap(Sitemap):
    changefreq = "monthly"

    def __init__(self, language):
        self.language = language

    def items(self):
        return Overview.published.all()

    def location(self, obj):
        # change locale in the url for the language for this sitemap
        try:
            from localeurl.templatetags.localeurl_tags import chlocale
            return chlocale(obj.get_absolute_url(), self.language)
        except:
            return obj.get_absolute_url()


