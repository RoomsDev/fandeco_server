# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.db.models.signals import post_save
from django.core.urlresolvers import reverse
from django.conf import settings
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import Site
from django.template.defaultfilters import striptags
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone

from pytils.translit import slugify

from .managers import OverviewPublishedManager


class Overview(models.Model):
    DRAFT = 0
    MODERATED = 1
    PUBLISHED = 2

    STATUS_CHOICES = ((DRAFT, _('draft')),
                  (MODERATED, _('moderated')),
                  (PUBLISHED, _('published')))

    author = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='overviews', verbose_name=_('author'))
    title = models.CharField(_('title'), max_length=254)
    slug = models.SlugField(verbose_name=_('slug'), help_text=_("Used to build the entry's URL."))
    body_html = models.TextField(_('HTML version'))
    description = models.TextField(_('description'), max_length=255, blank=True)
    image = models.ImageField(_('image'), upload_to='overviews/pics/%Y/%m/%d', null=True, blank=True,
            help_text=u"Для лучшего отображения на всех страницах сайта размер картинки предпочтителен 800х400 пикселей")

    # Какому объекту принадлежит обзор
    content_type = models.ForeignKey(ContentType, related_name='overviews')
    object_id = models.PositiveIntegerField(db_index=True)
    content_object = generic.GenericForeignKey('content_type', 'object_id',)

    status = models.IntegerField(
            _('status'), db_index=True,
            choices=STATUS_CHOICES, default=DRAFT)
    is_deleted = models.BooleanField(default=False, verbose_name=_('is a deleted'))
    is_pablished_email = models.BooleanField(default=False, verbose_name=_('is send email'))
    published_at = models.DateTimeField(_('published at'), default=timezone.now)
    created_at = models.DateTimeField(_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(_('updated at'), auto_now=True)

    objects = OverviewPublishedManager()

    class Meta:
        verbose_name = _('Overview')
        verbose_name_plural = _(u'Overviews')
        ordering = ["-created_at"]

    def __unicode__(self):
        return u"%s" % self.title

    def get_author(self):
        return self.author.get_full_name() if self.author.get_full_name() else self.author.username

    def get_absolute_url(self):
        return reverse("overviews_detail", kwargs={'slug':self.slug, 'pk': self.pk})

    def get_absolute_preview_url(self):
        return reverse("overviews_preview_detail", kwargs={'pk': self.pk})

    def save(self):
        if self.description is None:
            self.description = striptags(self.body_html)[:250]
        if not self.slug:
            self.slug = slugify(self.title)[:50]
        super(Overview, self).save()


# def Entry_update_counter_overviews(sender, **kwargs):
#     instance = kwargs['instance']
#     entry = Entry.objects.get(pk=instance.entry_id)
#     entry.count_overviews = entry.overviews.filter(status=PUBLISHED, is_deleted=False).count()
#     if instance.status == PUBLISHED and not instance.is_pablished_email:
#         context = {'obj':instance,
#                    'site': Site.objects.get_current()}
#         send_templated_mail(
#                   template_name='user_overview',
#                   from_email=settings.DEFAULT_FROM_EMAIL,
#                   recipient_list=[instance.author.email],
#                   context=context,
#                   )
#         instance.is_pablished_email = True
#         instance.save()
#     entry.save(update_fields=['count_overviews'])
#
# post_save.connect(Entry_update_counter_overviews, sender=Overview)
