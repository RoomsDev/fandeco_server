# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

import autocomplete_light
from ckeditor.widgets import CKEditorWidget
from treebeard.admin import TreeAdmin
from treebeard.forms import movenodeform_factory

from .models import Category, Product, Manufacturer, ProductImage, \
    Attribute, AttributeValue, TagPage
from django.contrib.admin.widgets import AdminFileWidget
from sorl.thumbnail.shortcuts import get_thumbnail
from django.forms.models import ModelForm
from django.utils.safestring import mark_safe


class AdminImageWidget(AdminFileWidget):
    def render(self, name, value, attrs=None):
        output = []
        if value and getattr(value, "url", None):
            t = get_thumbnail(value, '100x100')
            output.append('<img src="{}">'.format(t.url))
        output.append(super(AdminFileWidget, self).render(name, value, attrs))
        return mark_safe(u''.join(output))

class ImageForm(ModelForm):
    class Meta:
        model = ProductImage
        exclude=()
        widgets = {
          'file': AdminImageWidget,
        }


class ProductImageInline(admin.TabularInline):
    model = ProductImage
    fk_name = "product"
    form = ImageForm
    extra = 0


class AttributeValueInline(admin.TabularInline):
    model = AttributeValue
    #fields=('attr', 'product', 'value')
    form = autocomplete_light.modelform_factory(AttributeValue, exclude=[])
    fk_name = "product"
    extra = 0


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = 'id', 'get_admin_thumb', 'title', 'category', 'brand', 'price', 'part_number', 'upc'
    list_display_links = ('id', 'title',)
    list_filter = ('is_published', )
    search_fields = ('title', 'slug',  'part_number', 'upc',)
    date_hierarchy = 'created_at'
    readonly_fields=('created_at', 'updated_at', 'get_admin_thumb', )

    prepopulated_fields = {'slug': ('title',)}

    inlines = [
        AttributeValueInline, ProductImageInline
    ]

    form = autocomplete_light.modelform_factory(Product, exclude=[])

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name in ('body_html',):
            return db_field.formfield(widget=CKEditorWidget(config_name='admin_ckeditor'))
        return super(ProductAdmin, self).formfield_for_dbfield(db_field, **kwargs)



@admin.register(ProductImage)
class ProductImageAdmin(admin.ModelAdmin):
    list_display=('product', 'get_admin_thumb', 'file', 'is_main', 'alt', 'caption')


@admin.register(Attribute)
class AttributeAdmin(admin.ModelAdmin):
    list_display = ('name', 'code', 'type')
    prepopulated_fields = {'code': ('name',)}


@admin.register(AttributeValue)
class AttributeValueAdmin(admin.ModelAdmin):
    list_display = ('product', 'attr', 'value')



@admin.register(TagPage)
class TagPageAdmin(admin.ModelAdmin):
    list_display = ('title',)
    prepopulated_fields = {'slug': ('title',)}


@admin.register(Category)
class CategoryAdmin(TreeAdmin):
    #list_display = 'id', 'title'
    form = movenodeform_factory(Category)
    prepopulated_fields = {'slug': ('title',)}

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name in ('body_html',):
            return db_field.formfield(widget=CKEditorWidget(config_name='admin_ckeditor'))
        return super(CategoryAdmin, self).formfield_for_dbfield(db_field, **kwargs)


@admin.register(Manufacturer)
class ManufacturerAdmin(admin.ModelAdmin):
    list_display = 'id', 'title'
    prepopulated_fields = {'slug': ('title',)}

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name in ('body_html',):
            return db_field.formfield(widget=CKEditorWidget(config_name='admin_ckeditor'))
        return super(ManufacturerAdmin, self).formfield_for_dbfield(db_field, **kwargs)