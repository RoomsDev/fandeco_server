# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-04-22 15:40+0300\n"
"PO-Revision-Date: 2014-05-22 10:53+0300\n"
"Last-Translator:    <admin@admin.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Translated-Using: django-rosetta 0.7.4\n"

#: templates/catalog/product_detail.html:13
#, fuzzy
#| msgid "Add article"
msgid "Add to Cart"
msgstr "Добавити статтю"

#~ msgid "created at"
#~ msgstr "створено в "

#~ msgid "updated at"
#~ msgstr "оновлено в "

#~ msgid "wish list"
#~ msgstr "список бажань"

#~ msgid "Check this box if the wish list."
#~ msgstr "Встановіть цю повідмітку, якщо це список бажань."

#~ msgid "cart"
#~ msgstr "кошик"

#~ msgid "carts"
#~ msgstr "кошики"

#~ msgid "quantity"
#~ msgstr "кількість"

#~ msgid "unit price"
#~ msgstr "ціна за одиницю"

#~ msgid "business"
#~ msgstr "бізнес"

#~ msgid "item"
#~ msgstr "елемент"

#~ msgid "items"
#~ msgstr "елементи"

#~ msgid "Cart"
#~ msgstr "Кошик"

#~ msgid "Cart empty"
#~ msgstr "Кошик порожній"

#~ msgid "Checkout"
#~ msgstr "Оформити"

#~ msgid "Total"
#~ msgstr "Разом"

#~ msgid "Refresh"
#~ msgstr "Оновити"

#~ msgid "Remove"
#~ msgstr "Видалити"

#~ msgid "Product"
#~ msgstr "Товар"

#~ msgid "Price"
#~ msgstr "Ціна"

#~ msgid "Quantity"
#~ msgstr "Кількість"

#~ msgid "Sum"
#~ msgstr "Сума"

#~ msgid "Пожалуйста уменьшите размер файла до %s. Текущий размер %s"
#~ msgstr "Будь ласка зменшіть розмір файлу %s. Поточний розмір %s"

#~ msgid "Тип файла %s не поддерживается"
#~ msgstr "Тип файлу %s не підтримується"

#~ msgid "title"
#~ msgstr "Назва"

#~ msgid "used for publication"
#~ msgstr "використовується для  публікації"

#~ msgid "description"
#~ msgstr "опис"

#~ msgid "parent category"
#~ msgstr "вищестояща категорія"

#~ msgid "rate"
#~ msgstr "рейтинг"

#~ msgid "article category"
#~ msgstr "категорія статті"

#~ msgid "article categories"
#~ msgstr "категорії статтей"

#~ msgid "language"
#~ msgstr "мова"

#~ msgid "draft"
#~ msgstr "чернетка"

#~ msgid "hidden"
#~ msgstr "сховано"

#~ msgid "published"
#~ msgstr "опубліковано"

#~ msgid "author"
#~ msgstr "автор"

#~ msgid "HTML version"
#~ msgstr "HTML версія "

#~ msgid "article image"
#~ msgstr "зображення статті"

#~ msgid "article smart image"
#~ msgstr "маленьке зображення статті"

#~ msgid "positive votes"
#~ msgstr "позитивні голоси"

#~ msgid "negative votes"
#~ msgstr "негативні голоси"

#~ msgid "comment enabled"
#~ msgstr "коментарі дозволено"

#~ msgid "linkback enabled"
#~ msgstr "Зворотнє посилання включено"

#~ msgid "super post"
#~ msgstr "супер пост"

#~ msgid "Count commentaries"
#~ msgstr "Кількість коментарів"

#~ msgid "User IP"
#~ msgstr "IP користувача"

#~ msgid "source"
#~ msgstr "джерело"

#~ msgid "article"
#~ msgstr "стаття"

#~ msgid "articles"
#~ msgstr "статті"

#~ msgid "trans HTML version"
#~ msgstr "переклвдена версія HTML"

#~ msgid "image"
#~ msgstr "зображення"

#~ msgid "images"
#~ msgstr "зображення"

#~ msgid "Articles"
#~ msgstr "Статті"

#~ msgid "No entries"
#~ msgstr "Нема записів"

#~ msgid "No image"
#~ msgstr "Нема зображення"

#~ msgid "Home"
#~ msgstr "Головна"

#~ msgid "Heading of post"
#~ msgstr "Заголовок запису"

#~ msgid "Text of post"
#~ msgstr "Текст запису"

#~ msgid "Publicate"
#~ msgstr "Опублікувати"

#~ msgid "Clear form"
#~ msgstr "Очистити форму"

#~ msgid "Add Image for article"
#~ msgstr "Добавиди зображення до статті"

#~ msgid "Image for article"
#~ msgstr "зображення до статті"

#~ msgid "Image for post"
#~ msgstr "зображення до запису"

#~ msgid "Upload"
#~ msgstr "Закачати"

#~ msgid "Reply to"
#~ msgstr "Відповідь для"

#~ msgid "Leave a comment"
#~ msgstr "Написати коментарій"

#~ msgid "Stop Replying"
#~ msgstr "Припинити  відповідь"

#~ msgid "Reply"
#~ msgstr "Відповісти"

#~ msgid "Posted On"
#~ msgstr "Розміщено в"

#~ msgid "Author"
#~ msgstr "Автор"

#~ msgid "Comments"
#~ msgstr "Коментарі"
