# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0003_auto_20150424_1657'),
    ]

    operations = [
        migrations.CreateModel(
            name='TagPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='updated at')),
                ('is_published', models.BooleanField(default=True, verbose_name='published')),
                ('published_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='published at')),
                ('title', models.CharField(help_text='\u041d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u0435, \u043e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0435\u043c\u043e\u0435 \u043f\u0440\u0438 \u043f\u0440\u043e\u0441\u043c\u043e\u0442\u0440\u0435 \u043d\u0430 \u0441\u0430\u0439\u0442\u0435', max_length=240, verbose_name='\u041d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u0435')),
                ('slug', models.SlugField(unique=True, max_length=240)),
                ('body_html', models.TextField(null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('image', models.ImageField(upload_to='bshop/tag-page/cover/%Y/%m/%d', null=True, verbose_name='image', blank=True)),
                ('display_order', models.IntegerField(default=0)),
                ('categories', models.ManyToManyField(help_text='\u0420\u0430\u0437\u0440\u0435\u0448\u0430\u0435\u0442\u0441\u044f \u0443\u043a\u0430\u0437\u0430\u0442\u044c Category \u043a\u043e\u0442\u043e\u0440\u044b\u0435 \u0431\u0443\u0434\u0443\u0442 \u043f\u043e\u0434\u043e\u0431\u0440\u0430\u043d\u044b \u0434\u043b\u044f \u044d\u0442\u043e\u0439 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b \u0443\u0441\u043b\u0443\u0433\u0438', related_name='tagpages', verbose_name='Categories', to='catalog.Category', blank=True)),
            ],
            options={
                'ordering': ('display_order', 'id'),
                'verbose_name': 'tagged page',
                'verbose_name_plural': 'tagged pages',
            },
        ),
    ]
