# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible
from django.core.cache import cache
from django.core.urlresolvers import reverse

from bshop.core.models import TimeStampModel

try:
    User = settings.AUTH_USER_MODEL
except ImportError:
    from django.contrib.auth.models import User

try:
    from django.utils import timezone
except ImportError:
    from datetime import datetime as timezone

"""
https://github.com/jrief/django-shop-wishlists/tree/master/shop_wishlists
"""


@python_2_unicode_compatible
class Wishlist(TimeStampModel):
    """
    Each customer, ie. user may have one or more wishlists to remember items
    for later acquisition.
    """
    user = models.ForeignKey(User, related_name='wishlists')
    name = models.CharField(max_length=30, null=True, blank=True)
    notes = models.TextField(null=True, blank=True,
                help_text=_('Any optional notes on the item (size, color, etc.).'))

    class Meta(object):
        db_table = 'bshop_wishlist'
        verbose_name = _('Wishlist')
        verbose_name_plural = _('Wishlists')

    def __str__(self):
        return u"%s" % self.name
    
    def is_empty(self):
        return self.wl_items.count() == 0

    @property
    def get_display_id(self):
        return '%s-%s%s' % (self.created_at.strftime('%y'), self.created_at.strftime('%m'), self.id)

    def delete_item(self, item_id):
        """
        A simple convenience method to delete an item from the wishlist.
        """
        WishlistItem.objects.get(pk=item_id).delete()
        self.save()
    
    def save(self, *args, **kwargs):
        super(Wishlist, self).save(*args, **kwargs)
        cache_key = 'WISHLISTS_%s' % self.user.pk
        wishlist_set = [wishlist.pk for wishlist in self.user.wishlists.all()]
        cache.set(cache_key, wishlist_set)
    
    def get_absolute_url(self):
        return reverse('wishlist_items', args=[self.pk])


@python_2_unicode_compatible
class WishlistItem(models.Model):
    """
    This is a holder for the item in the wishlist.
    """
    wishlist = models.ForeignKey(Wishlist, verbose_name=_('Wishlist'),
                                 related_name="wl_items")

    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = generic.GenericForeignKey('content_type', 'object_id')

    notes = models.TextField(null=True, blank=True,
                help_text=_('Any optional notes on the item (size, color, etc.).'))

    class Meta(object):
        db_table = 'bshop_wishlist_item'
        verbose_name = _('Wishlist item')
        verbose_name_plural = _('Wishlist items')

    def __str__(self):
        return u'%s %s' % (self.product.__class__.__name__, self.product.pk)

    # product
    def get_product(self):
        return self.content_type.get_object_for_this_type(pk=self.object_id)

    def set_product(self, product):
        self.content_type = ContentType.objects.get_for_model(type(product),
                                                              for_concrete_model=False)
        self.object_id = product.pk

    product = property(get_product, set_product)