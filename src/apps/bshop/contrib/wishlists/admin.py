from django.contrib import admin


from .models import Wishlist, WishlistItem


@admin.register(Wishlist)
class WishlistAdmin(admin.ModelAdmin):
    """ Admin for Wishlist. """
    pass


@admin.register(WishlistItem)
class WishlistItemAdmin(admin.ModelAdmin):
    """ Admin for WishlistItem. """
    pass

