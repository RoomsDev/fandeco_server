from django.conf.urls import patterns, url

from .import views


urlpatterns = patterns('',
    url(r'^$',
        views.UserWishlistsView.as_view(), name='wishlist'),
    url(r'^(?P<wid>\d+)/$',
        views.WishlistView.as_view(), name='wishlist_items'),
    url(r'^add/$',
        views.WishlistCreateView.as_view(), name='wishlist_add'),
    url(r'^delete/(?P<pk>\d+)/$',
        views.WishlistDeleteView.as_view(), name='wishlist_delete'),
    url(r'^edit/(?P<pk>\d+)/$',
        views.WishlistEditView.as_view(), name='wishlist_edit'),
    url(r'^add-item/(?P<ctype>\d+)/(?P<oid>\d+)/$',
        views.WishlistAddView.as_view(), name='wishlist_add_item'),
    url(r'^add-item/(?P<ctype>\d+)/(?P<oid>\d+)/(?P<wid>\d+)/$',
        views.WishlistAddView.as_view(), name='wishlist_add_item_concrete'),
    url(r'^clear/(?P<wid>\d+)/$',
        views.WishlistClearView.as_view(), name='wishlist_clear'),
    url(r'^(?P<pk>\d+)/remove/$',
        views.WishlistRemoveView.as_view(), name='wishlist_remove'),
)
