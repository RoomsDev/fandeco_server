# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Wishlist',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='updated at')),
                ('name', models.CharField(max_length=30, null=True, blank=True)),
                ('notes', models.TextField(help_text='Any optional notes on the item (size, color, etc.).', null=True, blank=True)),
                ('user', models.ForeignKey(related_name='wishlists', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'bshop_wishlist',
                'verbose_name': 'Wishlist',
                'verbose_name_plural': 'Wishlists',
            },
        ),
        migrations.CreateModel(
            name='WishlistItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('object_id', models.PositiveIntegerField()),
                ('notes', models.TextField(help_text='Any optional notes on the item (size, color, etc.).', null=True, blank=True)),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
                ('wishlist', models.ForeignKey(related_name='wl_items', verbose_name='Wishlist', to='wishlists.Wishlist')),
            ],
            options={
                'db_table': 'bshop_wishlist_item',
                'verbose_name': 'Wishlist item',
                'verbose_name_plural': 'Wishlist items',
            },
        ),
    ]
