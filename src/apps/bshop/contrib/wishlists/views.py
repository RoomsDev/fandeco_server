from django.core.urlresolvers import reverse
from django.core.exceptions import ValidationError

from django.views.generic import ListView, CreateView, DeleteView, UpdateView, View
from django.http import HttpResponseRedirect, HttpResponse
from django.template.loader import render_to_string
from django.shortcuts import get_object_or_404

from django.contrib import messages
from django.contrib.auth.decorators import login_required

from django.utils.http import is_safe_url
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext_lazy as _

from .settings import wishlist_settings
from .models import WishlistItem, Wishlist
from .forms import WishlistItemForm, WishlistForm

from corelib.mixins import JsonViewMixin

import json


class WishlistViewMixin(object):
    """ Common mixin for WishlistItem. """

    model = WishlistItem
    form_class = WishlistItemForm

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        """ Require user login. """

        return super(WishlistViewMixin, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        """ Return only items for current user. """

        user = self.request.user
        assert user.is_authenticated(), \
            'View should not be accesible for unauthenticated users.'

        qs = super(WishlistViewMixin, self).get_queryset()
        kwargs = {'wishlist__user':user}
        if 'wid' in self.kwargs:
            kwargs['wishlist_id'] = self.kwargs['wid']

        return qs.filter(**kwargs)

    def get_success_url(self):
        """
        Allow specification of return URL using hidden form field or GET
        parameter, similar to auth views.

        1. Use 'next' POST parameter if available.
        2. Use 'next' GET parameter if available.
        3. Fall back to 'REDIRECT_URL' named URL, defaulting
           to 'wishlist' view.
        """

        redirect_to = self.request.POST.get(
            'next', self.request.GET.get('next', '')
        )

        if not is_safe_url(url=redirect_to, host=self.request.get_host()):
            redirect_to = reverse(wishlist_settings.REDIRECT_URL)

        return redirect_to
    
    def get_context_data(self, **kwargs):
        ctx = super(WishlistViewMixin, self).get_context_data(**kwargs)
        if 'wid' in self.kwargs:
            ctx['current_wishlist'] = get_object_or_404(Wishlist, pk=self.kwargs['wid'])
        return ctx


class WishlistView(WishlistViewMixin, JsonViewMixin, ListView):
    """ View providing list if WishlistItem's for User. """

    def get_template_names(self):
        if 'popup' in self.request.GET:
            return ['wishlists/includes/wishlist_tag_items.html']
        return super(ListView, self).get_template_names()


class WishlistClearView(WishlistViewMixin, ListView):
    """ Clear the wishlist. """

    template_name_suffix = '_confirm_clear'

    def post(self, *args, **kwargs):
        """ Delete and report to user. """

        # Delete all items
        self.get_queryset().delete()

        # Create message
#        messages.add_message(
#            self.request,
#            messages.SUCCESS,
#            u'The wishlist has been cleared.'
#        )

        # Return result of super
        return HttpResponseRedirect(self.get_success_url())
    
    def get_success_url(self):
        wishlist = get_object_or_404(Wishlist, pk=self.kwargs['wid'])
        return wishlist.get_absolute_url()


class WishlistAddView(WishlistViewMixin, View):
    """ Add item to the wishlist. """
    
    def get(self, request, *args, **kwargs):
        item = self.model(content_type_id=kwargs['ctype'], object_id=kwargs['oid'])
        if 'wid' in kwargs:
            item.wishlist = get_object_or_404(request.user.wishlists.all(), pk=kwargs['wid'])
        else:
            try:
                item.wishlist = request.user.wishlists.all()[0]
            except IndexError:
                wishlist = Wishlist(user = request.user, name=_("Default"))
                wishlist.save()
                item.wishlist = wishlist
        try:
            item = self.model.objects.get(content_type_id=kwargs['ctype'], object_id=kwargs['oid'], wishlist=item.wishlist)
        except self.model.DoesNotExist:
            item.save()
        html = render_to_string('wishlists/tags/user_wishlists.html', {'user':request.user})
        json_dump = json.dumps({'html': html})
        return HttpResponse(json_dump, status=200)

#    def form_valid(self, form):
#        """ Save and report to user. """
#
#        # Set user
#        form.instance.user = self.request.user
#
#        # Get created instance
#        self.object = form.save(commit=False)
#
#        # Validate uniqueness
#        try:
#            form.instance.validate_unique()
#
#        except ValidationError:
#            messages.add_message(
#                self.request, messages.ERROR,
#                u'Item {item} is already in the wishlist.'.format(
#                    item=unicode(form.instance.item)
#                )
#            )
#
#            # Return redirect to success URL
#            return HttpResponseRedirect(self.get_success_url())
#
#        # Call super
#        result = super(WishlistAddView, self).form_valid(form)
#
#        # Create message
#        messages.add_message(
#            self.request, messages.SUCCESS,
#            u'Item {item} has been added to the wishlist.'.format(
#                item=unicode(form.instance.item)
#            )
#        )
#
#        # Return super
#        return result


class WishlistRemoveView(WishlistViewMixin, DeleteView):
    """ Delete item from the wishlist. """

    def delete(self, request, *args, **kwargs):
        """ Delete and report to user. """

        # Call super
        result = super(WishlistRemoveView, self).delete(
            self, request, *args, **kwargs
        )

        # Create message
#        messages.add_message(
#            self.request,
#            messages.SUCCESS,
#            'Item {item} has been removed from the wishlist.'.format(
#                item=unicode(self.object)
#            )
#        )

        # Return result of super
        return result


class UserWishlistMixin(object):
    """ Common mixin for Wishlist. """

    model = Wishlist
    form_class = WishlistForm

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        """ Require user login. """

        return super(UserWishlistMixin, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        """ Return only items for current user. """

        user = self.request.user
        assert user.is_authenticated(), \
            'View should not be accesible for unauthenticated users.'

        qs = super(UserWishlistMixin, self).get_queryset()

        return qs.filter(user=user)
    
    def get_context_data(self, **kwargs):
        ctx = super(UserWishlistMixin, self).get_context_data(**kwargs)
        if not 'form' in ctx:
            ctx['form'] = self.form_class()
        return ctx
    
    def get_success_url(self):
        """
        Allow specification of return URL using hidden form field or GET
        parameter, similar to auth views.

        1. Use 'next' POST parameter if available.
        2. Use 'next' GET parameter if available.
        3. Fall back to 'REDIRECT_URL' named URL, defaulting
           to 'wishlist' view.
        """

        redirect_to = self.request.POST.get(
            'next', self.request.GET.get('next', '')
        )

        if not is_safe_url(url=redirect_to, host=self.request.get_host()):
            redirect_to = reverse(wishlist_settings.REDIRECT_URL)

        return redirect_to
    
    def get_object(self, queryset=None):
        return get_object_or_404(self.model, pk=self.kwargs['pk'], user=self.request.user)


class UserWishlistsView(UserWishlistMixin, ListView):
    """ View providing list of Wishlist's for User. """
    
    pass



class WishlistAddEditMixin(UserWishlistMixin):
    """ Create or rename existing Wishlist. """
    template_name = "wishlists/wishlist_form.html"
    
    def form_valid(self, form):
        # Set user
        form.instance.user = self.request.user

        # Get created instance
        self.object = form.save()
        
        html = render_to_string(
            "wishlists/includes/wishlist_list_item.html",
            {'wishlist':self.object,}
            )
        
        json_dump = json.dumps({'html': html})
        return HttpResponse(json_dump, status=200)
    
    def form_invalid(self, form):
        html = render_to_string(
            self.template_name,
            {'form':form}
            )
        
        json_dump = json.dumps({'html': html, 'errors':True})
        return HttpResponse(json_dump, status=200)


class WishlistCreateView(WishlistAddEditMixin, CreateView):
    """ Create new Wishlist for User. """
    
    pass


class WishlistEditView(WishlistAddEditMixin, UpdateView):
    """ Create new Wishlist for User. """
    
    pass


class WishlistDeleteView(UserWishlistMixin, DeleteView):
    """ Delete wishlist. """

    def delete(self, request, *args, **kwargs):
        """ Delete and report to user. """

        # Call super
        result = super(WishlistDeleteView, self).delete(
            self, request, *args, **kwargs
        )

        # Create message
#        messages.add_message(
#            self.request,
#            messages.SUCCESS,
#            'Item {item} has been removed from the wishlist.'.format(
#                item=unicode(self.object)
#            )
#        )

        # Return result of super
        return result
