# Create your views here.

import json

from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.template.context import RequestContext
from django.http.response import HttpResponse
from django.template.loader import render_to_string
from django.core.urlresolvers import reverse_lazy

from bshop.contrib.catalog.models import Product

from .models import Item, Cart
from .proxy import CartProxy


def order_product_end(request, **kwargs):
    if request.user.is_authenticated():
        cart = get_object_or_404(Cart, user=request.user)
    else:
        cart = request.cart.get_cart(request)
    cart_item_list = cart.item_set.all()
    return render_to_response('cart/order.html',
                {'cart_item_list':cart_item_list, 'cart':cart},
                context_instance=RequestContext(request))


def add_product_to_cart(request, quantity=1, **kwargs):
    cart = request.cart
    product = get_object_or_404(Product, pk=kwargs['pk'])
    cart.add(product, product.price, quantity)
    if request.is_ajax():
        template = 'cart/ajax/{0}'
        html = render_to_string(
            template.format('cart.html'),
            {'cart':cart.cart,
             'request':request,
             'is_ajax':True}
            )
        json_dump = json.dumps({'html': html, 'total_quantity':cart.cart.total_quantity()})
        return HttpResponse(json_dump, status=200)
    else:
        return redirect(reverse_lazy('cart_list'))


def remove_product_to_cart(request, **kwargs):
    is_reveal = True
    cart = request.cart
    product = get_object_or_404(Item, pk=kwargs['pk'])
    cart.remove_item(product.id)
    if Item.objects.filter(cart=cart.cart).count() < 1:
        is_reveal = False
    if request.is_ajax():
        template = 'cart/ajax/{0}'
        html = render_to_string(
                template.format('cart.html'),
                {'cart':cart.cart,
                 'request':request,
                'is_ajax':True}
                )
        json_dump = json.dumps({'html': html, 'is_reveal':is_reveal, 'total_quantity':cart.cart.total_quantity()})
        return HttpResponse(json_dump, status=200)
    else:
        return redirect(reverse_lazy('cart_list'))


def update_to_cart(request, **kwargs):
    is_reveal = True
    cart = request.cart
    product = get_object_or_404(Item, pk=kwargs['pk'])
    try:
        quantity = int(request.GET.get('quantity', None))
    except:
        quantity = 0
    if quantity < 1:
        cart.remove_item(product.pk)
    else:
        cart.update(product.pk, quantity)
    if Item.objects.filter(cart=cart.cart).count() < 1:
        is_reveal = False
    if request.is_ajax():
        template = 'cart/ajax/{0}'
        product = get_object_or_404(Item, pk=kwargs['pk'])
        html = render_to_string(
                template.format('update.html'),
                {'cart':cart.cart, 'request':request, 'is_ajax':True, 'product':product}
                )
        json_dump = json.dumps({'html': html, 'is_reveal':is_reveal, 'total_quantity':cart.cart.total_quantity()})
        return HttpResponse(json_dump, status=200)
    else:
        return redirect(reverse_lazy('cart_list'))


def remove_from_cart(request, product_id):
    cart = request.cart
    cart.remove(product_id)


def get_cart(request):
    cart = request.cart
    if request.is_ajax():
        template = 'cart/ajax/{0}'
        html = render_to_string(
                template.format('cart.html'),
                {'cart':cart.cart, 'request':request, 'is_ajax':True}
                )
        json_dump = json.dumps({'html': html})
        return HttpResponse(json_dump, status=200)
    else:
        return render_to_response('cart/cart.html',
                dict(cart=CartProxy(request)),
                context_instance=RequestContext(request))

def clear_cart(request):
    cart = request.cart
    cart.clear()
    if request.is_ajax():
        template = 'cart/ajax/{0}'
        html = render_to_string(
                template.format('cart.html'),
                {'cart':cart.cart, 'request':request, 'is_ajax':True}
                )
        json_dump = json.dumps({'html': html})
        return HttpResponse(json_dump, status=200)
    else:
        return redirect(reverse_lazy('cart_list'))
