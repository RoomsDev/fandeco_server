# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-04-22 15:40+0300\n"
"PO-Revision-Date: 2014-05-22 10:09+0300\n"
"Last-Translator:    <admin@admin.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Translated-Using: django-rosetta 0.7.4\n"

#: templates/cart/ajax/cart.html:7 templates/cart/cart.html:4
#: templates/cart/cart.html.py:7 templates/cart/cart.html:15
msgid "Cart"
msgstr "Корзина"

#: templates/cart/ajax/cart.html:19 templates/cart/cart.html:27
msgid "Cart empty"
msgstr "Корзина пустая"

#: templates/cart/ajax/cart.html:25 templates/cart/cart.html:33
msgid "Checkout"
msgstr "Оформить"

#: templates/cart/ajax/cart.html:27 templates/cart/cart.html:35
msgid "Total"
msgstr "Итого"

#: templates/cart/includes/body_cart.html:35
#: templates/cart/includes/body_cart.html:37
msgid "Refresh"
msgstr "Обновить"

#: templates/cart/includes/body_cart.html:54
#: templates/cart/includes/body_cart.html:56
msgid "Remove"
msgstr "Удалить"

#: templates/cart/includes/top_cart.html:3
msgid "Product"
msgstr "Продукт"

#: templates/cart/includes/top_cart.html:4
msgid "Price"
msgstr "Цена"

#: templates/cart/includes/top_cart.html:5
msgid "Quantity"
msgstr "Количество"

#: templates/cart/includes/top_cart.html:6
msgid "Sum"
msgstr "Сумма"

#~ msgid "created at"
#~ msgstr "создано в"

#~ msgid "updated at"
#~ msgstr "обновлен в"

#~ msgid "wish list"
#~ msgstr "список пожеланий"

#~ msgid "Check this box if the wish list."
#~ msgstr "Установите этот флажок, если это список пожеланий."

#~ msgid "cart"
#~ msgstr "корзина"

#~ msgid "carts"
#~ msgstr "корзины"

#~ msgid "quantity"
#~ msgstr "количество"

#~ msgid "unit price"
#~ msgstr "цена за единицу"

#~ msgid "business"
#~ msgstr "бизнесы"

#~ msgid "item"
#~ msgstr "позиция"

#~ msgid "items"
#~ msgstr "позиции"

#~ msgid "Query"
#~ msgstr "Запрос"

#~ msgid "Category"
#~ msgstr "Категория"

#~ msgid "All"
#~ msgstr "Все"

#~ msgid "title"
#~ msgstr "Название"

#~ msgid "used for publication"
#~ msgstr "используется для публикации"

#~ msgid "description"
#~ msgstr "описание"

#~ msgid "parent category"
#~ msgstr "родительская категория"

#~ msgid "rate"
#~ msgstr "рейтинг"

#~ msgid "Article category"
#~ msgstr "категория статьи"

#~ msgid "Article categories"
#~ msgstr "категория статьи"

#~ msgid "author"
#~ msgstr "автор"

#~ msgid "HTML version"
#~ msgstr "HTML версия"

#~ msgid "image"
#~ msgstr "изображение"

#~ msgid "draft"
#~ msgstr "черновик"

#~ msgid "published"
#~ msgstr "опубликовано"

#~ msgid "published at"
#~ msgstr "опубликовано в"

#~ msgid "source"
#~ msgstr "источник"

#~ msgid "Tags"
#~ msgstr "Теги"

#~ msgid "comment enabled"
#~ msgstr "комментарии доступны"

#~ msgid "Count commentaries"
#~ msgstr "Количество комментариев"

#~ msgid "latest comment added at"
#~ msgstr "последний комментарий добавлен в"

#~ msgid "viewed"
#~ msgstr "Просмотрено"

#~ msgid "User IP"
#~ msgstr "IP пользователя"

#~ msgid "Article"
#~ msgstr "Статья"

#~ msgid "Articles"
#~ msgstr "Статьи"

#~ msgid "article"
#~ msgstr "статья"

#~ msgid "Image"
#~ msgstr "изображение"

#~ msgid "Added new comment"
#~ msgstr "Добавить комментарий"

#~ msgid "Added new comment to article: %s"
#~ msgstr "Добавить картинку для статьи: %s"

#~ msgid "Reset"
#~ msgstr "Сброс"

#~ msgid "Add Image for article"
#~ msgstr "Добавить картинку для статьи"

#~ msgid "Add article"
#~ msgstr "Добавить статью"

#~ msgid "Image for article"
#~ msgstr "изображение для статьи"

#~ msgid "Image for post"
#~ msgstr "изображение для записи"

#~ msgid "No entries"
#~ msgstr "Нет записей"

#~ msgid "No categories yet."
#~ msgstr "Нет категорий"

#~ msgid "No entries yet."
#~ msgstr "Нет записей"

#~ msgid "Name"
#~ msgstr "Имя"

#~ msgid "Rubric"
#~ msgstr "Рубрика"

#~ msgid "news categories"
#~ msgstr "Категории новостей"

#~ msgid "positive votes"
#~ msgstr "позитивные голоса"

#~ msgid "negative votes"
#~ msgstr "негативные голоса"

#~ msgid "hidden"
#~ msgstr "скрытый"

#~ msgid "linkback enabled"
#~ msgstr "обратная ссылка доступна"

#~ msgid "super post"
#~ msgstr "супер пост"

#~ msgid "articles"
#~ msgstr "Статьи"

#~ msgid "Reply to"
#~ msgstr "Ответить"

#~ msgid "Stop Replying"
#~ msgstr "Остановить ответ"

#~ msgid "Reply"
#~ msgstr "Ответ"

#~ msgid "Previous entry"
#~ msgstr "Предыдущая запись"

#~ msgid "Next entry"
#~ msgstr "Следующая запись"

#~ msgid "Comments"
#~ msgstr "Комментарии"

#~ msgid "No avatar"
#~ msgstr "Нет аватара"

#~ msgid "language"
#~ msgstr "язык"

#~ msgid "article image"
#~ msgstr "изображение для статьи"

#~ msgid "article smart image"
#~ msgstr "маленькое изображение"

#~ msgid "trans HTML version"
#~ msgstr "переведенная HTML версия"

#~ msgid "images"
#~ msgstr "изображения"

#~ msgid "No image"
#~ msgstr "нет изображения"

#~ msgid "Home"
#~ msgstr "Главная"

#~ msgid "Heading of post"
#~ msgstr "Заглавие записи"

#~ msgid "Text of post"
#~ msgstr "Текст записи"

#~ msgid "Publicate"
#~ msgstr "Опубликовано"

#~ msgid "Clear form"
#~ msgstr "Очистить форму"

#~ msgid "Upload"
#~ msgstr "Загрузить"

#~ msgid "Posted On"
#~ msgstr "Размещено в"

#~ msgid "Author"
#~ msgstr "автор"
